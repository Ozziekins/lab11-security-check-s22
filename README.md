# Lab11 -- Security check

## Introduction

Finally, this is the last lab in this semester. You will or you already have been creating applications that are rather sensitive to security so you want to check that your application is secure enough. And maybe you think that you are not competent enough for security testing or maybe the cost of failure is rather big in your case, but usually you want to delegate this work to the other institution to let them say if the system is secure enough. And in this lab, you would be that institution. **_Let's roll!_**

## OWASP

[OWASP](https://owasp.org/) is Open Web Application Security Project. It is noncommercial organization that works to improve the security of software. It creates several products and tools, which we will use today and I hope you will at least take into account in future.

## OWASP Top 10

[OWASP Top 10](https://owasp.org/www-project-top-ten/) is a project that highlights 10 current biggest risks in software security. Highlighting that they make it visible. And forewarned is forearmed. As a software developer, you may at least consider that risks.

## OWASP Cheat Sheet Series

[OWASP CSS](https://cheatsheetseries.owasp.org/) is a project that accumulates the information about security splitted into very specific topics. And sometimes that might answer the question "How to make it secure?" according to specific topics (e.g. forgot password functionality).

## Google security check

Let's check how Google responds to the OWASP Cheatsheets of Forgot Password topic.

### Select the requirement from cheatsheets

E.g. how Google provides the forgot password feature and whether it "Return a consistent message for both existent and non-existent accounts."([link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=forgot%20password%20service%3A-,Return%20a%20consistent%20message%20for%20both%20existent%20and%20non%2Dexistent%20accounts.,-Ensure%20that%20the)). To check this we should only try to reset password for existent and unexistent user and compare result.

Note: _In your work is important to make link to particlular requirement in cheatsheet._

### Create and execute test case

Up to this moment we do not know what would be during testing forgot password feature at google.com. Not only real result, but even steps. But we can expect some steps in this process and some expected result:

1. Beforehand we have to know some existent account.
2. Open google.com page
3. Try to authenticate
4. Understand that we forgot the password
5. Try to reset password for some unexistent account
6. Remember the result
7. Go back and make the same for the prepared account
8. Remember the result
9. Expect that results for 2 users would be indistinguishable, or the intruder might process [user enumeration atack](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#:~:text=user%20enumeration%20attack)

Note: _You should not provide this high level steps, you might take it into account and go to next step to documenting test case execution_

Now lets try to execute that with documenting each step, to let yourself or someone another to reproduce it. During execution some unexpected issues might happen. It is ok, if the planned test case is not changd a lot just mention it into result section. Or if original plan changes a lot then return to it, review it, change if needed and continue.

| Test step                                                                     | Result                                                                                                                                                                                                                        |
| ----------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Open google.com                                                               | Ok                                                                                                                                                                                                                            |
| Push sign in button                                                           | Login page opened. If you have already logged in log out and repeate                                                                                                                                                          |
| Push forgot email                                                             | There is no forgot password, but we can push forgot email. It still starts password recovery                                                                                                                                  |
| Open new tab with sign in page                                                | I tried to input unexistent account and understand that i do not know whether the account exists. So I would try to register with new account without submiting the form, so i would be sure about email that does not exists |
| Push create account button                                                    | Ok                                                                                                                                                                                                                            |
| Push create for yourself tab                                                  | Ok, create account form opened                                                                                                                                                                                                |
| Input your existent account email into email field                            | Ok, field highlight red                                                                                                                                                                                                       |
| Try to change this email to make it unique                                    | Ok, also you can input some name and page offer you new unique email. Remember that email                                                                                                                                     |
| Return to forgot password page and input the unexistent email and push submit | Ok, form with first and last names opened                                                                                                                                                                                     |
| Input some first and last names and push submit                               | Since you do not know the names of unexistent account it is ok to imagine somehting                                                                                                                                           |
| See the result that there is no such an account                               | Ok, remember this result                                                                                                                                                                                                      |
| Push retry button                                                             | Ok, we in the start of forgot password process                                                                                                                                                                                |
| Input prepared existent email and push submit                                 | Ok, form with last and first name opened                                                                                                                                                                                      |
| Input first and last names and push submit                                    | Ok, since it is prepared account you know its first and last names                                                                                                                                                            |
| See the page with confirmation to send resetting email                        | Ok. Remember this result                                                                                                                                                                                                      |

Thats all, we tried 2 accounts and get 2 results, they are different so test case failed

### Analysis

Test case was failed, and according to different result intruders might process user enumeration attack and get database with existing users emails names and surnames, at least there is such an opportunity. Of course to brute force every email it require a lot of resources, but in this case it requires much more, because with email intruder need to pick up its name and surname. So even test case failed, that is not an security issue in given context.

Interesting thing that for my account, the email with resetting information would be sent on the same email(that i want to restore password)

Note: _Do not consider this result analysis as guide to action. Just see the test case result and provide your thoughts about_

## Homework

### Goal

To get familiar with OWASP CSS project by practicing limited security blackbox testing based on its materials. The default topics for testing is "Forgot password" and "Files Upload", but we are appeciate you to read another topics and select interesting one.

Note: _You are free to select topics or even combine topics, but remember that you should be able to test it using black box testing_

### Task

1. Pick a website that you can test using blackbox mode and put it into your solution and check that it is unique with previous submissions(web site and topic tested should be unique toghether, e.g. google.com might be tested on forgot password in one submission, and on file upload in another one). Like it was during [lab 9](https://gitlab.com/sqr-inno/lab10-exploratory-testing-and-ui-testing-s22#homework)(If it is hard for you to find such website just google "Forgot password" and you will see list of forgot password pages, just select one for yourself, or if you select another topic, then google it to find websites and pages with another features)
2. Find specific advice/requirement/standard or similar that you want to check whether your picked website follows it. By default look into ["Forgot password"](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html) and ["File upload"](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html) topics, because there are very straight forward checklists, with possible black box testing. But again, you are free(And I appreciate that) to select any topics (Be sure that you are able to ensure that during black box testing)
3. For entity from previous stage imagine the flow of test(you do not need to document it), its inputs, high level steps, and expected results.
4. Follow that flow with documenting each step and results
5. Comment the results of test cases. Is that everything ok, maybe it is a found vulnerability or weakness, or it might be intended weaking of the security protection in given context, what might be an alternative solution.(This is not a checklist that you must follow, just take it into account as line of thought)
6. Repeat from the first point(actually the website might be the same with different topic or entity from second point, and you do not need to mention web site each time) 4 times(the one from the lab is not counted).

### Notes

1. Test cases might be executed fully manually, using helper tools, or fully automated, but in this cases I prefer to see the source code or know the used tool.
2. I want to see the linkage between entities from points 2,3,4,5 and with the referenced place from OWASP CSS(hope the structure would not change between your submission and my evaluation)
3. Some advices might be easy to test(e.g. password form should contain minimum password length validation), but some of them might require a lof of efforts(e.g. Ensure that the time taken for the user response message is uniform for existent user and unexistent). So 4 tests is just a recomendation it might be higher or lower. Finnaly I can reject submission if see too less effort put into the work. So to write me directly to prereview selected topics and proposed flows might be good idea(but not mandatory).

# Tests

### 1. Forgot Password

[OWASP link](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)

| Test step                                             | Result                                                                                                                 | Principle |
| ----------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------- | --------- |
| open oreilly.com                                      | OK                                                                                                                     |           |
| go to Sign in                                         | OK                                                                                                                     |           |
| put in a random email and click continue              | proceeds to show the field for password                                                                                |           |
| click on Forgot Password                              | pop up with: "Enter the email address associated with your account, and we'll send you a link to reset your password." |           |
| click on Send link                                    | says link has been sent to the account                                                                                 |           |
| try to click Forgot Password again                    | says "Password Reset Sent" and does not allow any changes to be made                                                   |           |
| changed the email address to another random email     | says "Password Reset Sent" and does not allow any changes to be made, recognizes it all as from the same account       |           |
| reload the page and tried again with associated email | took about 21seconds to receive the email and the password reset is only valid for the next 10 hours.                  |           |
| tried to sign in without clicking the reset token     | signed in successfully                                                                                                 |           |

Overall, the URL token was generated

![alt](./images/1.png)

### 2. File Upload

[OWASP link](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html)

| Test step                             | Result                                                               | Principle                                                                |
| ------------------------------------- | -------------------------------------------------------------------- | ------------------------------------------------------------------------ |
| open file.io                          | OK                                                                   |                                                                          |
| go to upload page                     | OK, it seems that any extension is allowed, "Share any type of file" | List allowed extensions.                                                 |
| attempt to upload an rtf file         | OK, file link is ready and the name is different                     | Change the filename to something generated by the application            |
| tried uploading a file with 250 chars | it was accepted                                                      | Set a filename length limit. Restrict the allowed characters if possible |
| tried uploading a 5gb file            | file too large                                                       | Set a file size limit                                                    |

Concerning one more principle: Only allow authorized users to upload files, all this time i could upload without logging in

![alt](./images/2.png)

### 3. Error handling

[OWASP link](https://cheatsheetseries.owasp.org/cheatsheets/Error_Handling_Cheat_Sheet.html)

In order to trigger error messages I did the following:

- search for random files and folders that will not be found (404s)
- request folders that exist and see the server behavior (403s, blank page, or directory listing).
- sending a request that breaks the HTTP RFC. One example would be to send a very large path, break the headers format, or change the HTTP version.

| Test step                                                                                             | Result                                                              |
| ----------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------- |
| Open coursera.com                                                                                     | Domain changed to .org                                              |
| Edit the path to be coursera.org/garbage                                                              | "We were not able to find the page you're looking for. "            |
| Made it a bit longer coursera.org/garbage/for/sqrs/lab/test                                           | Same error message                                                  |
| Made it a bit longer coursera.org/course/to/test                                                      | Redirects to coursera.org/browse?source=deprecated_spark_cdp        |
| Go to Explore tab and choose take a free course                                                       |                                                                     |
| Filter courses and choose one: "Agile with Atlassian Jira"                                            |                                                                     |
| Try to enrol without logging in                                                                       | "Please provide an email in the input below."                       |
| Go to search bar and enter: "how to be an ozzie that is known as the best status in the entire world" | "We couldn't find any exact matches for All Products related to..." |
|                                                                                                       | also took some time to load                                         |
| Go to Online degrees and coursera.org/degrees/random                                                  | "Sorry We couldn't find the degree that you're looking for.         |

The key thing in error handling is that the outcome when an unexpected error occurs should be a generic response returned by the application while the error details are logged server side for investigation, and NOT returned to the user.

![alt](./images/3.png)

### 4. Input Validation

[OWASP link](https://cheatsheetseries.owasp.org/cheatsheets/Input_Validation_Cheat_Sheet.html)

In order to validate input, i focused on email address validation on two levels:

- syntactic
- semantic

| Test step                                                                                         | Result                              | Syntax                                                                                                               |
| ------------------------------------------------------------------------------------------------- | ----------------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| Open calendly.com                                                                                 | Ok                                  |                                                                                                                      |
| user-001@hot@mail,com                                                                             | Please enter a valid email address. | The email address contains two parts, separated with an @ symbol.                                                    |
| user\001@hotmail.com                                                                              | Please enter a valid email address. | The email address does not contain dangerous characters (such as backticks, single or double quotes, or null bytes). |
| user.001@hotmail.com                                                                              | OK                                  | The domain part contains only letters, numbers, hyphens (-) and periods (.)                                          |
| AntidisestablishmentarianismFloccinaucinihilipilificationPseudopseudohypoparathyroidism@gmail.com | OK                                  | The local part (before the @) should be no more than 63 characters.                                                  |
| Put in a super super long address                                                                 | OK                                  | The total length should be no more than 254 characters.                                                              |

Of course a lot more combinations can be tested. However, from the above, the only one violated is length of characters. no matter how much I put, it was accepted

![alt](./images/4.png)

| Test step                                          | Result                                                | Semantic                                                                             |
| -------------------------------------------------- | ----------------------------------------------------- | ------------------------------------------------------------------------------------ |
| Open calendly.com                                  | Ok                                                    |                                                                                      |
| Put in my email address                            | OK, said a confirmation link was sent to my email     | The email address is correct and the application can successfully send emails to it. |
| Put in my name and password                        | all good, made sure password is at least 8 characters |                                                                                      |
| Click on sign up                                   | OK, i received the email                              | The user has access to the mailbox.                                                  |
| Copied the confirmation link and pasted in browser | Asked for password and was successful                 |                                                                                      |
| Pasted again in the browser in incognito mode      | Continued registeration from where it stopped         |                                                                                      |

All good here

### 5. User Privacy Protection

[OWASP link](https://cheatsheetseries.owasp.org/cheatsheets/User_Privacy_Protection_Cheat_Sheet.html)

| Test step                                               | Result                                                                                  |
| ------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| open gatesnotes.com                                     | OK                                                                                      |
| log in to existing account                              | OK                                                                                      |
| Go to Privacy Policies                                  | OK                                                                                      |
| check specification of information collection           | found in section 2                                                                      |
| confirm ability to delete personal information          | found in section 8, although it makes reference to section 11 below that does not exist |
| go to Profile settings and deactivate the account       | automatically logs me out                                                               |
| attempt to sign in again                                | OK                                                                                      |
| Make sure that the possibility to delete account exists | It does, but may take some days to process, so it is inconclusive                       |

Tbh I don't think this is some blackbox testing but I was exploring the cheat sheet and did something with it

![alt](./images/5.png)
